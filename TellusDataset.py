'''
Created on 05/06/2012

@author: Mads
'''


from appfuncs.AppFunc import AppFunc
from appfuncs.filemenu.open_menu import FileOpener

from model.Attribute import Attribute
from model.Dataset import Dataset
import numpy as np


class OpenTellus(AppFunc, FileOpener):

    def __init__(self, controller, gui, model):
        AppFunc.__init__(self, controller, gui, model, "Tellus", ["File", "Open"], ['Open'], 15)

    def run(self, filename=None, dtype=None):
        if filename is None:
            return self.get_filenames_and_open("Open Tellus file", "*", dtype)
        else:
            return TellusDataset(self.model, filename, dtype)

    def is_applicable(self, dataItemList):
        return len(dataItemList) == 0

class TellusDataset(Dataset):

    """Extends Dataset with constructor that loads the specified *.csv file"""

    names = ["Velocity", "Lvelocity", "Sideslip", "Attack", "Acc2", "Acc3", "Missing name",
             "Edge1", "Flap1", "Rodflap1",
             "Edge2", "Flap2", "Rodflap2",
             "Edge3", "Flap3", "Rodflap3",
             "Sonicx", "Sonicy", "Sonicw", "Sonict", "Sgnorth",
             "Temp", "Sgeast", "Rotpos", "Rpm", "Status", "Sgwest", "Elpow", "Wspeed", "Wdir", "Yaw", "Airpress"]
    units = ['Volt', 'Volt', 'Volt', 'Volt', 'Volt', 'Volt', 'Volt', 'Volt', 'Volt', 'Volt',
             'Volt', 'Volt', 'Volt', 'Volt', 'Volt', 'Volt', 'Volt', 'Volt', 'Volt', 'Volt',
             'Volt', 'Deg', 'Volt', 'Volt', 'rpm', 'Volt', 'Volt', 'Kw', 'm/s', 'Deg',
             'Deg', 'mBar']
    gains = np.array([1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,
                      1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,
                      1., 6., 1., 1., 15.8255, 1., 1., 30., 3.84553, 100.,
                      36., 30.1])
    offsets = np.array([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                        0., 0., 0., 0., 0., 0., 0., 0., 0., 0.,
                        0., 5., 0., 0., 0., 0., 0., 0., 19.4513, 0.,
                        180.000, 940.670])
    descriptions = ['Local relative velocity', 'Lvelocity', 'Local sideslip', 'Local angle of attack', 'Flapwise acceleration at seg.2', 'Flapwise acceleration at seg.3',
                    'dummy',
                    'Edgewise force at seg.1?', 'Flapwise force at seg.1?', 'Flapwise blade root moment of blade 1',
                    'Edgewise force at seg.2?', 'Flapwise force at seg.2?', 'Flapwise blade root moment of blade 2',
                    'Edgewise force at seg.3?', 'Flapwise force at seg.3?', 'Flapwise blade root moment of blade 3',
                    'Sonic wind speed', 'Sonicy', 'Sonicw', 'Sonict', 'Tension in tower pole (north)',
                    'Air temperature', 'Tension in tower pole (east)', 'Rotor position (zero for blade one vertical upwards)', 'Rotor speed', 'Status', 'Tension in tower pole (west)',
                    'Electrical power', 'Anemometer wind speed', 'Wind direction', 'Yaw position', 'Air pressure']  #rotor torque


    def __init__(self, model, file_object, dtype=None):
        """
        model: DataModel object
        file: file (absolute or relative path to data file) or FtpFile
        """
        Dataset.__init__(self, model, file_object=file_object)
        i = 0; self.gains[i], self.offsets[i], self.units[i] = -15.352731, 13.440767, 'm/s'  # Wrong in some files
        i = 3; self.gains[i], self.offsets[i], self.units[i] = -1, 0, '-Volt'  # Wrong in some files
        i = 9; self.gains[i], self.offsets[i], self.units[i] = 16.134755, -2.751942, 'kNm'  # flap1
        i = 12; self.gains[i], self.offsets[i], self.units[i] = 15.432086, -8.313435, 'kNm'  #flap2
        i = 15; self.gains[i], self.offsets[i], self.units[i] = 15.758704, 0.158717, 'kNm'  #flap3
        i = 16; self.gains[i], self.offsets[i], self.units[i] = 10.192999, 0.554210, 'm/s'  #Sonic wind speed
        i = 28; self.gains[i], self.offsets[i], self.units[i] = 1.924358, 19.454070, 'm/s'  #Sonic wind speed

        self.load()

    def _load(self):
        data = np.fromfile(self.filename, dtype=np.float32).reshape((15000, 32)).astype(self.dtype)
        data = data * self.gains + self.offsets
        attributes = [Attribute(self, name, unit, desc) for name, unit, desc in zip(self.names, self.units, self.descriptions)]
        self.set_data(data, attributes)

    def get_info(self):
        return Dataset.get_info(self) + "\nFilename:\t%s" % self.filename
