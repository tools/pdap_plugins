'''
Created on 05/06/2012

@author: Mads
'''
from appfuncs.AppFunc import AppFunc
from appfuncs.filemenu.open_menu import FileOpener

from model.Attribute import Attribute
from model.Dataset import Dataset
import numpy as np


class OpenHawcAtTime(AppFunc, FileOpener):

    def __init__(self, controller, gui, model):
        AppFunc.__init__(self, controller, gui, model, "HAWC2 output_at_time", ["File", "Open"], ['Open'], 15)

    def run(self, filename=None, dtype=None):
        if filename is None:
            return self.get_filenames_and_open("Open NewFormat data file", "*.dat", dtype)
        else:
            return AtTimeDataset(self.model, filename, dtype)

    def is_applicable(self, dataItemList):
        return len(dataItemList) == 0

class AtTimeDataset(Dataset):

    def __init__(self, model, file_object, dtype=None):
        Dataset.__init__(self, model, file_object=file_object)
        self.load()

    def _load(self):
        with open(self.filename) as fid :
            _ = fid.readline()  # Version ID : HAWC2AERO 2.4w
            self.description = fid.readline()[3:]  # " # Radial output at time:    1.00000000000000"
            names = [s.strip() for s in fid.readline().split("#")[1:]]  # " # Radius_s   # twist      # Chord"
            lines = fid.readlines()
        data = np.array([np.fromstring(l, sep=" ") for l in lines])
        attributes = [Attribute(self, name) for name in names]
        self.set_data(data, attributes)
