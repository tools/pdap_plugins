
from appfuncs.AppFunc import AppFunc
from appfuncs.filemenu.open_menu import FileOpener
import numpy as np
from model.Attribute import Attribute
class OpenMyDatasetExample(AppFunc, FileOpener):

    def __init__(self, controller, gui, model):
        AppFunc.__init__(self, controller, gui, model, name="Open my dataset",main_menu_pos=["Files",'Open'],popup_menu_pos=["Open"])

    def run(self, filename=None, dtype=None):
        if filename is None:
            return self.get_filenames_and_open("Open NewFormat data file", "*.*", dtype)
        else:
            ds = self.controller.AddDataset(name="name", description="")
            sensornames = ['n1','n2']
            sensorunits = ['u1','u2']
            sensordescriptions = ['d1','d2']
            data = np.arange(20).reshape(10,2)
            #or
            sensor_data1 = np.arange(10)
            sensor_data2 = np.arange(10)+1
            data = [sensor_data1, sensor_data2]
            attributes = [Attribute(ds, n,u,d) for n,u,d in zip(sensornames,sensorunits, sensordescriptions)]
            ds.set_data(data, attributes)
            return ds
