from appfuncs.AppFunc import AppFunc
from appfuncs.filemenu.open_menu import FileOpener

from model.Attribute import Attribute
from model.Dataset import Dataset
import numpy as np


class OpenBladed(AppFunc, FileOpener):

    def __init__(self, controller, gui, model):
        AppFunc.__init__(self, controller, gui, model, "Bladed", ["File", "Open"], ['Open'], 15)
        try:
            self.controller.Open.register_file_extension(".%*", "OpenBladed")
        except:
            pass

    def run(self, filename=None, dtype=None):
        if filename is None:
            return self.get_filenames_and_open("Open Bladed data file", "*.%*", dtype)
        else:
            return BladedDataset(self.model, filename, dtype)

    def is_applicable(self, dataItemList):
        return len(dataItemList) == 0

class BladedDataset(Dataset):

    def __init__(self, model, file_object, dtype=None):

        Dataset.__init__(self, model, file_object=file_object)

        self.load()

    def _load(self):
        filename = self.filename
        with open(filename) as fid :
            info = {}
            words = None
            for l in fid:
                if l.startswith("   "):
                    info[words[0].upper()].extend(l.split())
                else:
                    words = []
                    for i, v in enumerate(l.split("'")):
                        if i % 2 == 0:
                            words.extend(v.split())
                        else:
                            words.append(v)

                    if len(words) == 2:
                        info[words[0].upper()] = words[1]
                    else:
                        info[words[0].upper()] = words[1:]
        ndims = int(info['NDIMENS'])
        if ndims > 3:
            raise NotImplementedError
        dims = np.roll([int(v) for v in info['DIMENS']], 1)

        data = np.empty((dims[0], np.prod(dims[1:])), dtype=self.dtype)

        if info['ACCESS'].upper() == 'D':
            with open(filename.replace('%', '$'), 'rb') as fid:
                dtype = eval("np." + ("float", "int")[info['FORMAT'].upper()[0] == 'I'] + str(int(info['FORMAT'].upper()[-1]) * 8))
                fid.seek(int(info.get('HEADREC', '0')))
                data = np.fromfile(fid, dtype, np.prod(dims)).reshape((dims[0], np.prod(dims[1:])))
        if info['ACCESS'].upper() == 'S':
            with open(filename.replace('%', '$')) as fid:
                for _ in range(int(info.get('HEADREC', '0'))):
                    fid.readline()
                data = np.fromfile(fid, sep=" ").reshape(data.shape)



        gain = np.array(info.get('VARSCALE', 1), np.float64)
        offset = np.array(info.get('VAROFFSET', 0), np.float64)
        if (np.any(gain != 1) or np.any(offset != 0)) and ndims > 2:
            raise NotImplementedError("gain and offset must be repeated")
        data = gain * (data - offset)

        names = info['VARIAB']
        units = info['VARUNIT']
        if ndims == 3:
            names = ["%s_%d" % (n, i) for i in range(1, dims[-1] + 1) for n in names ]
            units = units * dims[1]

        basis_att_name = info['AXISLAB'].replace("'", "")
        if basis_att_name.lower() == "time":
            basis_att_name = "Time"
        basis_att_unit = info['AXIUNIT']
        if info['AXIMETH'] == "2":
            basis_att_start = float(info['MIN'])
            basis_att_step = float(info['STEP'])
            data = np.concatenate([np.atleast_2d(np.arange(dims[0]) * basis_att_step + basis_att_step).T, data], 1)
        else:
            raise NotImplementedError

        names = [basis_att_name] + names
        units = [basis_att_unit] + units

        attributes = [Attribute(self, name, unit) for name, unit in zip(names, units)]
        self.set_data(data, attributes)
